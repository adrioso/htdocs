<?php
    require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
    class Buscar_Service{
        public function consultar_torneos_cantidad($cantidad){
            if($cantidad<1){
                echo "Debe seleccionar una cantidad mayor a 0 <br>";
            }else{

            
            $conne = Conectar::conn();
            $sql = "SELECT codigo, nombre_juego,fecha_inicio, fecha_fin, estado, tipo_torneo, cuenta FROM (
                SELECT a.codigo codigo, a.fecha_inicio fecha_inicio, a.fecha_fin fecha_fin, a.estado estado, a.tipo_torneo tipo_torneo, a.nombre_juego nombre_juego, count(b.codigo_encuentro) cuenta FROM `torneo` a, `encuentro` b, `participacion` c
                WHERE a.codigo = b.id_torneo
                  AND c.encuentro = b.codigo_encuentro
                GROUP BY codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego
                ORDER BY count(b.id_torneo) DESC
                ) tab
                WHERE tab.cuenta > $cantidad";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los torneos <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>codigo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre de juego</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>fecha inicio</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha fin</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Estado</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo torneo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Número de participaciones</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre_juego"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_inicio"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_fin"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["estado"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_torneo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cuenta"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }
        }

        public function consultar_torneos_fecha($fecha_desde, $fecha_hasta){
            if($fecha_desde>=$fecha_hasta){
                echo "Por favor seleccione dos fechas validas<br>";
            }else{

            
            $conne = Conectar::conn();
            $sql = "SELECT a.codigo codigo, a.fecha_inicio fecha_inicio, a.fecha_fin fecha_fin, a.estado estado, a.tipo_torneo tipo_torneo, a.nombre_juego nombre_juego FROM `torneo` a
            WHERE a.fecha_fin BETWEEN CAST('$fecha_desde' AS datetime) AND CAST('$fecha_hasta' AS datetime)";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los torneos <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>codigo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>fecha inicio</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha fin</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Estado</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo torneo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre de juego</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_inicio"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_fin"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["estado"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_torneo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre_juego"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }
        }



    }
