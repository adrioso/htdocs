<?php
require $_SERVER['DOCUMENT_ROOT'] . "\db-project\conexion.php";
class Participacion_Service
{
    public function insertar_participacion($encuentro, $competidor, $equipo)
    {
        $conne = Conectar::conn();

        $sql1 = "SELECT tipo_torneo FROM torneo,encuentro WHERE encuentro.id_torneo = torneo.codigo AND encuentro.codigo_encuentro = $encuentro";
        $datos = mysqli_query($conne, $sql1);
        if (($conne->error)) {
            echo "Se ha producido un error al consultar la informacion de las participaciones <br>";
            echo $conne->errno . "=" . $conne->error . "<br>";
        } else {

            $fila = mysqli_fetch_array($datos);
            $fila = $fila["tipo_torneo"];

            if ($competidor <> "" and  $equipo <> "") {
                echo    "<script type='text/javascript'>
                                alert('Debe seleccionar un equipo o un competidor individual, no ambos');
                                window.history.go(-1);
                            </script>";
            }
            elseif($fila == "I" and $competidor == ""){
                echo    "<script type='text/javascript'>
                                alert('El torneo es individual, seleccione un competidor');
                                window.history.go(-1);
                            </script>";
            } 
            elseif($fila == "E" and $equipo == ""){
                echo    "<script type='text/javascript'>
                                alert('El torneo es en equipos, seleccione un equipo');
                                window.history.go(-1);
                            </script>";
            }else {

                $sql = "";

                echo    "<script type='text/javascript'>
                                alert('$fila');
                                window.history.go(-1);
                            </script>";

                if ($fila == "I") {
                    $sql = "INSERT INTO participacion (encuentro, competidor, tipo_participacion) VALUES ('$encuentro', '$competidor', '$fila')";
                } else {
                    $sql = "INSERT INTO participacion (encuentro, equipo,  tipo_participacion) VALUES ('$encuentro', '$equipo', '$fila')";
                }

                if (!mysqli_query($conne, $sql)) {
                    echo "Se ha producido un error al registrar el encuentro <br>";
                    echo $conne->errno . "=" . $conne->error . "<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                } else {
                    echo    "<script type='text/javascript'>
                                alert('El encuentro ha sido registrado correctamente');
                                window.history.go(-1);
                            </script>";
                }
            }
        }
    }

    public function mostrar_participacion()
    {
        $conne = Conectar::conn();
        $sql = "SELECT codigo_participacion, tipo_participacion, competidor, equipo, encuentro FROM `participacion`";

        $datos = mysqli_query($conne, $sql);

        if (($conne->error)) {
            echo "Se ha producido un error al consultar la informacion de las participaciones <br>";
            echo $conne->errno . "=" . $conne->error . "<br>";
        } else {
            echo "<table>";
            echo "<tr>";
            echo "<td><b>Código participacion</b></td>";
            echo "<td><b>Código encuentro</b></td>";
            echo "<td><b>Tipo participacion</b></td>";
            echo "<td><b>Competidor individual</b></td>";
            echo "<td><b>Equipo</b></td>";
            echo "</tr>";
            while ($fila = mysqli_fetch_array($datos)) {
                echo "<tr>";
                echo "<td>" . $fila["codigo_participacion"] . "</td>";
                echo "<td>" . $fila["encuentro"] . "</td>";
                echo "<td>" . $fila["tipo_participacion"] . "</td>";
                echo "<td>" . $fila["competidor"] . "</td>";
                echo "<td>" . $fila["equipo"] . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
    }

    public function borrar_participacion($codigo_participacion)
    {
        $conne = Conectar::conn();
        $sql = "DELETE  
                      FROM participacion 
                     WHERE participacion.codigo_participacion = $codigo_participacion";
        if (!mysqli_query($conne, $sql)) {
            echo "Se ha producido un error al borrar la participacion <br>";
            echo $conne->errno . "=" . $conne->error . "<br>";
            echo "<button onClick='history.back()'>Regresar</button>";
        } else {
            echo    "<script type='text/javascript'>
                                alert('La participacion ha sido borrado correctamente');
                                window.history.go(-1);
                            </script>";
        }
    }
}
