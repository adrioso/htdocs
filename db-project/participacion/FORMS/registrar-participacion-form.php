<!DOCTYPE html>
<html>
    <head>
        <title>Registro Participacion</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR PARTICIPACIONES</h1>
                <h2 align="center" style="color: white">Registrar</h2>
            </div>
        
            <div class="scrollmenu">
                    <a href="/db-project/participacion/gestionar-participacion.php">Inicio Gestion</a>
                    <a href="/db-project/participacion/FORMS/eliminar-participacion-form.php">Eliminar participacion</a>
                    <a href="/db-project/admin/FORMS/buscar-admin-form.php">Buscar</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
                <br>
                <form method="POST" action="/db-project/participacion/CRUD/registrar-participacion.php">
                    <table>
                    <tr>
                        <th align="left">Código encuentro:<br></th>
                        <th colspan=2><select name="codigo_encuentro" id="codigo_encuentro" required>
                        <?php
                                require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo_encuentro, id_torneo, fecha_realizacion FROM `encuentro`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los encuentros <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo_encuentro'].'">'.$fila['codigo_encuentro'].' - '.$fila['id_torneo'].' - '.$fila['fecha_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></th>
                    </tr>
                    <tr>
                        <th align="left">Código competidor individual:<br></th>
                        <th colspan=2><select name="competidor" id="competidor">
                        <?php
                                
                                $conne = Conectar::conn();
                                $sql = "SELECT codcompetidor, nickname FROM `competidor`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los competidores <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    echo '<option ></option>';
                                    while ($fila =mysqli_fetch_array($datos)) {
                                        
                                    echo '<option value="'.$fila['codcompetidor'].'">'.$fila['codcompetidor'].' - '.$fila['nickname'].' - '.$fila['lugar_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></th>
                    </tr>
                    <tr>
                        <th align="left">Código equipo:<br></th>
                        <th colspan=2><select name="equipo" id="equipo" >
                        <?php
                               
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo_equipo, nombre_equipo FROM `equipo`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los equipos <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    echo '<option ></option>';
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo_equipo'].'">'.$fila['codigo_equipo'].' - '.$fila['nombre_equipo'].' - '.$fila['lugar_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></th>
                    </tr>
                    <tr>
                        <th colspan=2><input align = "center" type="submit" value="Registrar"></th>
                    </tr>
                    
                    </table>
                </form>
            </div>
        </div>
        
    </body>
</html>