<!DOCTYPE html>
<html>
    <head>
        <title>Eliminar participaciones</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR PARTICIPACIONES</h1>
                <h2 align="center" style="color: white">Eliminar</h2>
            </div>
        
            <div class="scrollmenu">
                    <a href="/db-project/participacion/gestionar-participacion.php">Inicio Gestion</a>
                    <a href="/db-project/participacion/FORMS/registrar-participacion-form.php">Registrar participacion</a>
                    <a href="/db-project/admin/FORMS/buscar-admin-form.php">Buscar</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
                <br>
                <form method="POST" action="/db-project/participacion/CRUD/eliminar-participacion-id.php">
                    <table>
                    <tr><select name="codigo_participacion" required>
                            <?php
                                require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo_participacion, tipo_participacion, competidor, equipo FROM `participacion`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de las participaciones <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo_participacion'].'">'.$fila['codigo_participacion'].' - Tipo:'.$fila['tipo_participacion'].' - Competidor:'.$fila['competidor'].' - Equipo:'.$fila['equipo'].'</option>';
                                    }
                                }
                            ?>
                    </select></tr>
                    <tr><input type="submit" name="eliminar" value="Eliminar"></tr>
                    </table>
                </form>
            </div>
        </div>
        
    </body>
</html>