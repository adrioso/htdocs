<!DOCTYPE html>
<html>
    <head>
        <title>Gestionar participaciones</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR PATRICIPACIONES</h1>
                <h2 align="center" style="color: white">Menu</h2>
            </div>

            <div class="scrollmenu">
                <a href="/db-project/index.html">Inicio</a>
                <a href="/db-project/participacion/FORMS/registrar-participacion-form.php">Registrar participacion</a>
                <a href="/db-project/participacion/FORMS/eliminar-participacion-form.php">Eliminar participacion</a>
            </div>
        </div>
        <div>
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/db-project/participacion/CRUD/participacion-service.php");
                $nuevo = new Participacion_Service();
                $nuevo -> mostrar_participacion();
            ?>
        </div>
    </body>
</html>
