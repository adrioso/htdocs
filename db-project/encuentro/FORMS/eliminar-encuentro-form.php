<!DOCTYPE html>
<html>
    <head>
        <title>Eliminar encuentros</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR ENCUENTROS</h1>
                <h2 align="center" style="color: white">Eliminar</h2>
            </div>
        
            <div class="scrollmenu">
                    <a href="/db-project/encuentro/gestionar-encuentro.php">Inicio Gestion</a>
                    <a href="/db-project/encuentro/FORMS/registrar-encuentro-form.php">Registrar encuentro</a>
                    <a href="/db-project/admin/FORMS/buscar-admin-form.php">Buscar</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
                <br>
                <form method="POST" action="/db-project/encuentro/CRUD/eliminar-encuentro-id.php">
                    <table>
                    <tr><select name="codigo_encuentro" required>
                            <?php
                                require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo_encuentro, fecha_realizacion, id_torneo, estado_encuentro FROM `encuentro`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los encuentros <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo_encuentro'].'">'.$fila['codigo_encuentro'].' - Torneo:'.$fila['id_torneo'].' - Fecha:'.$fila['fecha_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></tr>
                    <tr><input type="submit" name="eliminar" value="Eliminar"></tr>
                    </table>
                </form>
            </div>
        </div>
        
    </body>
</html>