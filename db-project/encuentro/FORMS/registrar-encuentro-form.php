<!DOCTYPE html>
<html>
    <head>
        <title>Registro Administrador</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR ENCUENTROS</h1>
                <h2 align="center" style="color: white">Registrar</h2>
            </div>
        
            <div class="scrollmenu">
                    <a href="/db-project/encuentro/gestionar-encuentro.php">Inicio Gestion</a>
                    <a href="/db-project/encuentro/FORMS/eliminar-encuentro-form.php">Eliminar encuentro</a>
                    <a href="/db-project/admin/FORMS/buscar-admin-form.php">Buscar</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
                <br>
                <form method="POST" action="/db-project/encuentro/CRUD/registrar-encuentro.php">
                    <table>
                    <tr>
                        <th align="left">Fecha:<br></th>
                        <th><input type="datetime-local" name="fecha_realizacion" required><br></th>
                    </tr>
                    <tr>
                        <th align="left">Torneo:<br></th>
                        <th colspan=2><select name="id_torneo" required>
                        <?php
                                require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo, fecha_inicio, fecha_fin, tipo_torneo, nombre_juego, lugar_realizacion FROM `torneo`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los torneos <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo'].'">'.$fila['codigo'].' - '.$fila['nombre_juego'].' - '.$fila['lugar_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></th>
                    </tr>
                    <tr>
                        <th colspan=2><input align = "center" type="submit" value="Registrar"></th>
                    </tr>
                    
                    </table>
                </form>
            </div>
        </div>
        
    </body>
</html>