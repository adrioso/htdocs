<!DOCTYPE html>
<html>
    <head>
        <title>Gestionar encuentros</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR ENCUENTROS</h1>
                <h2 align="center" style="color: white">Menu</h2>
            </div>

            <div class="scrollmenu">
                <a href="/db-project/index.html">Inicio</a>
                <a href="/db-project/encuentro/FORMS/registrar-encuentro-form.php">Registrar encuentro</a>
                <a href="/db-project/encuentro/FORMS/eliminar-encuentro-form.php">Eliminar encuentro</a>
            </div>
        </div>
        <div>
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/db-project/encuentro/CRUD/encuentro-service.php");
                $nuevo = new encuentro_Service();
                $nuevo -> mostrar_encuentro();
            ?>
        </div>
    </body>
</html>
