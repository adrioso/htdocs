<?php
    require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
    class Encuentro_Service{
        public function insertar_encuentro ($fecha_realizacion, $id_torneo){
                $conne = Conectar::conn();
                $sql = "INSERT INTO encuentro (fecha_realizacion, estado_encuentro, id_torneo) VALUES ('$fecha_realizacion', 'PA', '$id_torneo')";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al registrar el encuentro <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El encuentro ha sido registrado correctamente');
                                window.history.go(-1);
                            </script>";
                }

        }

        public function mostrar_encuentro(){
            $conne = Conectar::conn();
            $sql = "SELECT codigo_encuentro, fecha_realizacion, estado_encuentro, ganador, puntaje_ganador, id_torneo FROM `encuentro`";

            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al consultar la informacion de los encuentros <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td><b>Código encuentro</b></td>";
                        echo "<td><b>Torneo</b></td>";
                        echo "<td><b>Fecha Agenda</b></td>";
                        echo "<td><b>Estado</b></td>";
                        echo "<td><b>Ganador</b></td>";
                        echo "<td><b>Puntaje ganador</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td>".$fila ["codigo_encuentro"]."</td>";
                        echo "<td>".$fila ["id_torneo"]."</td>";
                        echo "<td>".$fila ["fecha_realizacion"]."</td>";
                        echo "<td>".$fila ["estado_encuentro"]."</td>";
                        echo "<td>".$fila ["ganador"]."</td>";
                        echo "<td>".$fila ["puntaje_ganador"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function borrar_encuentro ($codigo_encuentro){
            $conne = Conectar::conn();
            $sql = "DELETE  
                      FROM encuentro 
                     WHERE encuentro.codigo_encuentro = $codigo_encuentro";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al borrar el encuentro <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El encuentro ha sido borrado correctamente');
                                window.history.go(-1);
                            </script>";
                }
        }
    }
?>