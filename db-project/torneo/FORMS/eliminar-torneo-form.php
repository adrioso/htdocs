<!DOCTYPE html>
<html>
    <head>
        <title>Eliminar torneo</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR TORNEOS</h1>
                <h2 align="center" style="color: white">Eliminar</h2>
            </div>
        
            <div class="scrollmenu">
                    <a href="/db-project/torneo/gestionar-torneo.php">Inicio Gestion</a>
                    <a href="/db-project/torneo/FORMS/registrar-torneo-form.html">Registrar torneos</a>
                    <a href="/db-project/torneo/FORMS/consultar-torneo-form.php">Consultar</a>
                    <a href="/db-project/torneo/FORMS/buscar-torneo-form.php">Buscar</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
                <br>
                <form method="POST" action="/db-project/torneo/CRUD/eliminar-torneo-id.php">
                    <table>
                    <tr><select name="codigo" required>
                            <?php
                                require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
                                $conne = Conectar::conn();
                                $sql = "SELECT codigo, fecha_inicio, fecha_fin, tipo_torneo, nombre_juego, lugar_realizacion FROM `torneo`";
                
                                $datos = mysqli_query($conne, $sql);
                
                                if(($conne -> error)){
                                   echo "Se ha producido un error al consultar la informacion de los torneos <br>";
                                   echo $conne -> errno ."=". $conne -> error ."<br>";
                                }
                                else{
                                    while ($fila =mysqli_fetch_array($datos)) {
                                    echo '<option value="'.$fila['codigo'].'">'.$fila['codigo'].' - '.$fila['nombre_juego'].' - '.$fila['lugar_realizacion'].'</option>';
                                    }
                                }
                            ?>
                    </select></tr>
                    <tr><input type="submit" name="eliminar" value="Eliminar"></tr>
                    <table>
                </form>
            </div>
        </div>
        
    </body>
</html>