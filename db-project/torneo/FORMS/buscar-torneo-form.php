<!DOCTYPE html>
<html>
    <head>
        <title>Buscar</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
        <?php
        include($_SERVER['DOCUMENT_ROOT']."/db-project/buscar/buscar-service.php");
        $nuevo = new Buscar_Service();
        ?>
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR TORNEOS</h1>
                <h2 align="center" style="color: white">Buscar</h2>
            </div>
        
            <div class="scrollmenu">
                <a href="/db-project/torneo/gestionar-torneo.php">Inicio Gestion</a>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
            <br>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    
                    <input type="number" name="cantidad" required>
                    <br>
                    <input type="submit" name="buscar_torneos_cantidad" value="Buscar torneos con n mas participaciones">
            </form>
                <br>
                <?php
                    
                    if (isset($_POST['buscar_torneos_cantidad'])){
                        $nuevo->consultar_torneos_cantidad($_POST["cantidad"]);
                    }
                ?>
            </div>
            <br>
            <div>
            <br>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    
                    <input type="datetime-local" name="date_desde" required>
                    <input type="datetime-local" name="date_hasta" required>
                    <br>
                    <input type="submit" name="buscar_torneos_fecha" value="Buscar torneos con fecha de finalizacion entre las fechas seleccionadas">
            </form>
                <br>
                <?php
                    if (isset($_POST['buscar_torneos_fecha'])){
                        $nuevo->consultar_torneos_fecha($_POST["date_desde"], $_POST["date_hasta"]);
                    }
                ?>
            </div>
        </div>
        
    </body>
</html>