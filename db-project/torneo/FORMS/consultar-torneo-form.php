<!DOCTYPE html>
<html>
    <head>
        <title>Consulta torneos</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR TORNEOS</h1>
                <h2 align="center" style="color: white">Consultar</h2>
            </div>
        
            <div class="scrollmenu">
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <a href="/db-project/torneo/gestionar-torneo.php">Inicio Gestion</a>
                    <a><input type="submit" name="masencuentros" value="Torneo con más encuentros"></a>
                    <a><input type="submit" name="masparticipaciones" value="Torneo con más participaciones"></a>
                </form>
            </div>
            </div>
        </div>
        <div align = "center">
            <div>
            <br>
                <?php
                    include($_SERVER['DOCUMENT_ROOT']."/db-project/torneo/CRUD/torneo-service.php");
                    $nuevo = new Torneo_Service();
                    if (isset($_POST['masencuentros'])){                        
                        $nuevo -> consultar_torneo_encuentros();
                    }
                    elseif (isset($_POST['masparticipaciones'])){
                        $nuevo -> consultar_torneo_participaciones();
                    }
                ?>
            </div>
        </div>
        
    </body>
</html>