<!DOCTYPE html>
<html>
    <head>
        <title>Gestionar torneos</title>
        <link rel="stylesheet" type="text/css" href="style.css" >
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="titulo">
            <div>
                <br>
                <h1 align="center" style="color: white">GESTIONAR TORNEOS</h1>
                <h2 align="center" style="color: white">Menu</h2>
            </div>

            <div class="scrollmenu">
                <a href="/db-project/index.html">Inicio</a>
                <a href="/db-project/torneo/FORMS/registrar-torneo-form.html">Registrar torneo</a>
                <a href="/db-project/torneo/FORMS/eliminar-torneo-form.php">Eliminar torneo</a>
                <a href="/db-project/torneo/FORMS/consultar-torneo-form.php">Consultar</a>
                <a href="/db-project/torneo/FORMS/buscar-torneo-form.php">Buscar</a>
            </div>
        </div>
        <div>
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/db-project/torneo/CRUD/torneo-service.php");
                $nuevo = new Torneo_Service();
                $nuevo -> mostrar_torneo();
            ?>
        </div>
    </body>
</html>
