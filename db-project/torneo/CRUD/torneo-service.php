<?php
    require $_SERVER['DOCUMENT_ROOT'] ."\db-project\conexion.php" ;
    class Torneo_Service{
        public function insertar_torneo ($fecha_inicio, $fecha_fin, $tipo_torneo, $nombre_juego, $lugar_realizacion, $asientos_disp ){
                $conne = Conectar::conn();
                $sql = "INSERT INTO torneo (fecha_inicio, fecha_fin, tipo_torneo, nombre_juego, lugar_realizacion, asientos_disp, estado) VALUES 
                                          ('$fecha_inicio', '$fecha_fin', '$tipo_torneo', '$nombre_juego', '$lugar_realizacion', '$asientos_disp', 'PA')";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al registrar el torneo <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El torneo ha sido registrado correctamente');
                                window.history.go(-1);
                            </script>";
                }

        }

        public function mostrar_torneo(){
            $conne = Conectar::conn();
            $sql = "SELECT codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego, lugar_realizacion, asientos_disp FROM `torneo`";

            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al consultar la informacion de los administradores <br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td><b>Código</b></td>";
                        echo "<td><b>Fecha Inicio</b></td>";
                        echo "<td><b>Fecha Fin</b></td>";
                        echo "<td><b>Estado</b></td>";
                        echo "<td><b>Tipo torneo</b></td>";
                        echo "<td><b>Juego</b></td>";
                        echo "<td><b>Lugar</b></td>";
                        echo "<td><b>Asientos disponibles</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td>".$fila ["codigo"]."</td>";
                        echo "<td>".$fila ["fecha_inicio"]."</td>";
                        echo "<td>".$fila ["fecha_fin"]."</td>";
                        echo "<td>".$fila ["estado"]."</td>";
                        echo "<td>".$fila ["tipo_torneo"]."</td>";
                        echo "<td>".$fila ["nombre_juego"]."</td>";
                        echo "<td>".$fila ["lugar_realizacion"]."</td>";
                        echo "<td>".$fila ["asientos_disp"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function borrar_torneo ($codigo){
            $conne = Conectar::conn();
            $sql = "DELETE  
                      FROM torneo 
                     WHERE torneo.codigo = $codigo";
                if(!mysqli_query($conne, $sql)){
                    echo "Se ha producido un error al eliminar el torneo <br>";
                    echo $conne -> errno ."=". $conne -> error ."<br>";
                    echo "<button onClick='history.back()'>Regresar</button>";
                }
                else{
                    echo    "<script type='text/javascript'>
                                alert('El torneo ha sido borrado correctamente');
                                window.history.go(-1);
                            </script>";
                }
        }

        public function consultar_torneo_encuentros(){
            $conne = Conectar::conn();
            $sql = "SELECT c.codigo codigo, c.fecha_inicio fecha_inicio, c.fecha_fin fecha_fin, c.estado estado, c.tipo_torneo tipo_torneo, c.nombre_juego nombre_juego, count(d.id_torneo) cuenta FROM `torneo` c, `encuentro` d
            WHERE c.codigo = d.id_torneo
            GROUP BY codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego
            HAVING count(d.id_torneo) = (SELECT count(b.id_torneo) cuenta FROM `torneo` a, `encuentro` b
            WHERE a.codigo = b.id_torneo
            GROUP BY codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego
            ORDER BY count(b.id_torneo) DESC
            LIMIT 1)
                    ";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los torneos<br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>codigo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>fecha inicio</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha fin</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Estado</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo torneo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre de juego</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Número de encuentros</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_inicio"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_fin"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["estado"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_torneo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre_juego"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cuenta"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }

        public function consultar_torneo_participaciones(){
            $conne = Conectar::conn();
            $sql = "SELECT d.codigo codigo, d.fecha_inicio fecha_inicio, d.fecha_fin fecha_fin, d.estado estado, d.tipo_torneo tipo_torneo, d.nombre_juego nombre_juego, count(f.codigo_participacion) cuenta FROM `torneo` d, `encuentro` e, `participacion` f
            WHERE d.codigo = e.id_torneo
              AND f.encuentro = e.codigo_encuentro
            GROUP BY codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego
            HAVING count(f.codigo_participacion) = (SELECT count(b.codigo_encuentro) cuenta FROM `torneo` a, `encuentro` b, `participacion` c
            WHERE a.codigo = b.id_torneo
              AND c.encuentro = b.codigo_encuentro
            GROUP BY codigo, fecha_inicio, fecha_fin, estado, tipo_torneo, nombre_juego
            ORDER BY count(b.id_torneo) DESC
            LIMIT 1)";
            $datos = mysqli_query($conne, $sql);

            if(($conne -> error)){
                echo "Se ha producido un error al obtener la informacion de los torneos<br>";
                echo $conne -> errno ."=". $conne -> error ."<br>";
            }
            else{
                echo "<table>";
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>codigo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>fecha inicio</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Fecha fin</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Estado</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Tipo torneo</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Nombre de juego</b></td>";
                        echo "<td align='center' style = 'border: black 1px solid'><b>Número de participaciones</b></td>";
                    echo "</tr>";
                while ($fila =mysqli_fetch_array($datos)){
                    echo "<tr>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["codigo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_inicio"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["fecha_fin"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["estado"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["tipo_torneo"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["nombre_juego"]."</td>";
                        echo "<td align='center' style = 'border: black 1px solid'>".$fila ["cuenta"]."</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }


    }
